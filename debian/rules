#!/usr/bin/make -f

UPSTREAM_GIT = https://github.com/openstack/ironic-inspector.git
include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=pybuild --with python3

override_dh_auto_clean:
	python3 setup.py clean
	rm -rf build .stestr *.egg-info debian/*.templates debian/po
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done

override_dh_clean:
	dh_clean
	rm -rf build debian/ironic-inspector.postinst debian/ironic-inspector.config debian/ironic-inspector.postrm
	rm -rf debian/*.service debian/*.init

override_dh_auto_build:
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func ironic-inspector.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func ironic-inspector.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_postrm ironic-inspector.postrm
	pkgos-merge-templates ironic-inspector ironic-inspector db ksat endpoint

override_dh_auto_test:
	echo "Do nothing..."

override_dh_auto_install:
	echo "Do nothing..."

override_dh_install:
	for i in $(PYTHON3S) ; do \
		python$$i setup.py install --install-layout=deb --root $(CURDIR)/debian/tmp ; \
	done

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	pkgos-dh_auto_test --no-py2 'ironic_inspector\.test\.unit\.(?!(test_migrations\.TestMigrationSqlite\.test_upgrade_and_version|test_migrations\.TestMigrationSqlite\.test_walk_versions|test_manager\.TestManagerDelHost\.test_del_host_with_coordinator))'
endif

	mkdir -p $(CURDIR)/debian/ironic-inspector/usr/share/ironic-inspector
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file $(CURDIR)/debian/ironic-inspector/usr/share/ironic-inspector/inspector.conf \
		--namespace ironic_inspector \
		--namespace ironic_lib.mdns \
		--namespace keystonemiddleware.auth_token \
		--namespace oslo.db \
		--namespace oslo.log \
		--namespace oslo.messaging \
		--namespace oslo.middleware.cors \
		--namespace oslo.middleware.healthcheck \
		--namespace oslo.policy \
		--namespace oslo.service.service \
		--namespace oslo.service.sslutils \
		--namespace oslo.service.wsgi
	pkgos-readd-keystone-authtoken-missing-options $(CURDIR)/debian/ironic-inspector/usr/share/ironic-inspector/inspector.conf keystone_authtoken ironic-inspector

	mkdir -p $(CURDIR)/debian/ironic-inspector/etc/ironic-inspector/policy.d
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslopolicy-sample-generator \
		--output-file $(CURDIR)/debian/ironic-inspector/etc/ironic-inspector/policy.d/00_default_policy.yaml \
		--format yaml \
		--namespace ironic_inspector.api

	# Comment out deprecated policies
	sed -i 's/^"/#"/' $(CURDIR)/debian/ironic-inspector/etc/ironic-inspector/policy.d/00_default_policy.yaml

	# Use the policy.d folder
	pkgos-fix-config-default $(CURDIR)/debian/ironic-inspector/usr/share/ironic-inspector/inspector.conf oslo_policy policy_dirs /etc/nova/policy.d

	# Debian uses the "standalone" mode, meaming one ironic-inspector-conductor daemon, and a wsgi server (using uwsgi).
	# If not set, none of these 2 daemons will start.
	pkgos-fix-config-default $(CURDIR)/debian/ironic-inspector/usr/share/ironic-inspector/inspector.conf DEFAULT standalone False

	sed -i 's|^[ \t#]*connection[ \t]=.*|connection = sqlite:////var/lib/ironic-inspector/inspectordb|' $(CURDIR)/debian/ironic-inspector/usr/share/ironic-inspector/inspector.conf
	install -D -m 0644 rootwrap.conf $(CURDIR)/debian/ironic-inspector/etc/ironic-inspector/rootwrap.conf
	install -D -m 0644 rootwrap.d/ironic-inspector.filters $(CURDIR)/debian/ironic-inspector/etc/ironic-inspector/rootwrap.d/ironic-inspector.filters
	install -D -m 0644 $(CURDIR)/debian/ironic-inspector.sudoers $(CURDIR)/debian/ironic-inspector/etc/sudoers.d/ironic-inspector
	sed -i 's|stack|ironic-inspector|' $(CURDIR)/debian/ironic-inspector/etc/sudoers.d/ironic-inspector

	dh_install
	dh_missing --fail-missing

# We use override_dh_installmenu because it's done in the sequence with dh,
# and we already override dh_installinit in pkgos.make.
# Obviously, we will never use a Desktop menu in such a package, so that's
# not a problem.
override_dh_installmenu:
	dh_installinit -pironic-inspector --name=ironic-inspector-api
	dh_installinit -pironic-inspector --name=ironic-inspector-conductor

override_dh_installsystemd:
	dh_installsystemd -pironic-inspector --name=ironic-inspector-api
	dh_installsystemd -pironic-inspector --name=ironic-inspector-conductor

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
